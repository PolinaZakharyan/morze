SEP = ' '

ALPHABET = {'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.', 'G': '--.', 'H': '....', 'I': '..', 'J': '.---', 'K': '-.-', 'L': '.-..', 'M': '--', 'N': '-.', 'O': '---', 'P': '.--.', 'Q': '--.-', 'R': '.-.', 'S': '...', 'T': '-', 'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-', 'Y': '-.--', 'Z': '--..',
'1': '.----', '2': '..---', '3': '...--',
'4': '....-', '5': '.....', '6': '-....',
'7': '--...', '8': '---..', '9': '----.', '0': '-----', ', ': '--..--', '.': '.-.-.-', '?': '..--..', '/': '-..-.', '-': '-....-', '(': '-.--.', ')': '-.--.-'}



def encode_letter(letter):
    return ALPHABET.get(letter.upper(), letter)

def encode_word(word):
    return SEP.join((encode_letter(letter) for letter in word))

def encode_string(string):
    return SEP.join((encode_word(word) for word in string.split()))


def code_morze(value):
    '''
    please add your solution here or call your solution implemented in different function from here
    then change return value from 'False' to value that will be returned by your solution
    '''
    return encode_string(value)
